<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        try{
            $rs = array();

            $categories = DB::table( 'categories' )->get();

            foreach ($categories as $key => $category) {
                array_push($rs, array(
                    'id' => $category->id,
                    'text' => $category->name,
                    'value' => $category->id,
                    'checked' => $key == 0 ? true : false,
                    'description' => $category->description,
                    
                ));
            } 

            return Response()->json( array( 'code' => "1", 'categories' => $rs ) );
           
            // DB::commit();
             
        }catch(\Illuminate\Database\QueryException  $e){
            // DB::rollback();
            return Response()->json( array('code' => '-1', 'msg' => 'Oops Something goes wrong.', 'error' => $e->getMessage() ) );
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
