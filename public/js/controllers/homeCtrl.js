var angular = require('angular');

angular.module('library')
.controller('homeCtrl', [ '$scope','$location', 'homeService','$rootScope', '$uibModal', 
function( $scope, $location, homeService, $rootScope, $uibModal){

	vm = this;

	vm.books = [
	    // {category:'Fantasy', id:'1', name: "Harry Potter and the Philosopher's stone", source: 'HP_1.jpeg', borrowed: 'available', publishedDate: '2017-05-01', author:'J. K. Rowling', user: '' },
	    // {category:'Fantasy', id:'2', name: 'Harry Potter and the Chamber of secrets', source: 'HP_2.jpeg', borrowed: 'borrowed', publishedDate: '2017-05-01', author:'J. K. Rowling', user: 'Oscar' },
	    // {category:'Fantasy', id:'3', name: 'Harry Potter and the Prisioner of Azkabam', source: 'HP_3.jpeg', borrowed: 'available', publishedDate: '2017-05-01', author:'J. K. Rowling', user: '' },
	    // {category:'Fantasy', id:'4', name: 'Harry Potter and the Globet of Fire', source: 'HP_4.jpeg', borrowed: 'borrowed', publishedDate: '2017-05-01', author:'J. K. Rowling', user: 'Francisco' },
	    // {category:'Fantasy', id:'5', name: 'Harry Potter and the Order of the Phoenix', source: 'HP_5.jpeg', borrowed: 'available', publishedDate: '2017-05-01', author:'J. K. Rowling', user: '' },
	    // {category:'Fantasy', id:'6', name: 'Harry Potter and the Half-Blood Prince', source: 'HP_6.jpeg', borrowed: 'borrowed', publishedDate: '2017-05-01', author:'J. K. Rowling', user: 'Andalon' },
	    // {category:'Fantasy', id:'7', name: 'Harry Potter and the Deathly Hallows', source: 'HP_7.jpeg', borrowed: 'available', publishedDate: '2017-05-01', author:'J. K. Rowling', user: '' },
	    // {category:'Drama', id:'3', name: 'Guittar', source='HP_3', borrowed:'false'},
	    // {category:'Drama', id:'2', name:'bass', borrowed:'false'},
	    // {category:'Drama', id:'2', name:'bass', borrowed:'false'},
	    // {category:'Drama', id:'2', name:'bass', borrowed:'false'},
	    // {category:'Drama', id:'2', name:'bass', borrowed:'false'},
	    // {category:'Drama', id:'2', name:'bass', borrowed:'false'}
	];
	
  	vm.viewby = 10;
  	vm.totalItems = vm.books.length;
  	vm.currentPage = 1;
  	vm.itemsPerPage = vm.viewby;
  	vm.maxSize = 5; 

  	vm.setPage = function (pageNo) {
	    vm.currentPage = pageNo;
	};

  	vm.pageChanged = function() {
	    console.log('Page changed to: ' + vm.currentPage);
	};

	vm.setItemsPerPage = function(num) {
	  vm.itemsPerPage = num;
	  vm.currentPage = 1; //reset to first page
	}

	vm.book = {
		delete : function( id ){
			$.SmartMessageBox({
                title: 'Do you really want to delete this book?',
                content: 'Please, Choose an option.',
                buttons: '[No][Yes]'
            }, function(ButtonPressed) {
                if (ButtonPressed === "Yes") {
                	vm.deleteBook( id );
                }

            });
		},
		update : function( book ){
			$uibModal.open({
                templateUrl : 'udpBookMdl.html',
                controller : 'udpBookMdlCtrl',
                // scope: $scope,
                size: 'lg',
                backdrop:'static',
                keyboard:false,
                resolve: {
                    Book: book
                }
            });
		},
		updateBook : function(){
			$.SmartMessageBox({
                title: 'Do you really want to update this book?',
                content: 'Please, Choose an option.',
                buttons: '[No][Yes]'
            }, function(ButtonPressed) {
                if (ButtonPressed === "Yes") {
					successMSG('Great!','Operation Successfully Completed');

                }else{
                	errorMSG('Oops','Something goes wrong.');

                }

            });

		},
		add : function(){
			$uibModal.open({
                templateUrl : 'addBookMdl.html',
                controller : 'addBookMdlCtrl',
                scope: $scope,
                size: 'lg',
                backdrop:'static',
                keyboard:false
            });

		},
		addBook : function(){
			$.SmartMessageBox({
                title: 'Do you really want to add this book?',
                content: 'Please, Choose an option.',
                buttons: '[No][Yes]'
            }, function(ButtonPressed) {
                if (ButtonPressed === "Yes") {
					successMSG('Great!','Operation Successfully Completed');

                }else{
                	errorMSG('Oops','Something goes wrong.');

                }

            });

		},

	}

	$rootScope.$on('reloadBooks', function(){
		vm.loadBooks();

	});

	vm.loadBooks = function(){
		homeService.books.get()
		.then(function( response ){
			if(response.data.code == 1){
				vm.books = response.data.books;
				setTimeout(function() {
					vm.totalItems = response.data.books.length;
					vm.viewby = 10;
					$scope.$apply();
					
				}, 10);
			}else{
				errorMSG('Oops','Something goes Wrong.');
				
			}

		})
		.catch(function( response ){
			errorMSG('Oops','Something goes Wrong.');
			
		});

	}

	vm.deleteBook = function( id ){

		homeService.books.delete( id )
		.then(function( response ){
			if(response.data.code == 1){
				vm.loadBooks();
				successMSG('Great!','Operation Successfully Completed');

			}else{
				errorMSG('Oops','Something goes Wrong.');
				
			}

		})
		.catch(function( response ){
			errorMSG('Oops','Something goes Wrong.');
			
		});
	}	

	vm.search = '';

}]);	