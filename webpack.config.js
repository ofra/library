// var webpack = require('webpack');

// module.exports = {
// 	context: __dirname,
// 	entry: {
// 		app: './public/js/app.js'
// 	},
// 	output: {
// 		path: __dirname + '/public/js/built',
// 		filename: 'app.bundle.js'
// 	}
// };

var debug = process.env.NODE_ENV !== "production";
var webpack = require('webpack');
var path = require('path');
module.exports = {
    context: path.join(__dirname, "public"),
    devtool: debug ? "inline-sourcemap" : null,
    entry: "./js/app.js",
    module: {
        loaders: [
            {
                exclude: /(node_modules|bower_components)/,	
                
            },
            { test: /\.css$/, loader: 'style-loader!css-loader' },
            {
                test: /\.(eot|svg|ttf|woff|woff2)$/,
                loader: 'url-loader?limit=100000' 
            }
        ]
    },
    output: {
        path: __dirname + "/public/js/built/",
        filename: "app.bundle.js"
    },
    plugins: debug ? [] : [
        new webpack.optimize.DedupePlugin(),
        new webpack.optimize.OccurenceOrderPlugin(),
        new webpack.optimize.UglifyJsPlugin({ mangle: false, sourcemap: false }),
    ],
    node: { fs: 'empty' }
};