<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     *
     * Run the database seeds.
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert([
	        "name" =>  "Harry Potter and the Philosopher's stone",
	        "author" => 'J. K. Rowling',
	        "user" =>  'Oscar',
	        "publishedDate" =>  '2017-05-01',
	        "image" =>  'HP_1.jpeg',
	        "category_id" => 1,
	        'created_at' => date("Y-m-d H:i:s")
        ]);

	    DB::table('books')->insert([
	    	"name" =>  'Harry Potter and the Chamber of secrets',
	    	"author" => 'J. K. Rowling',
	    	"user" =>  'Francisco',
	    	"publishedDate" =>  '2017-05-01',
	    	"image" =>  'HP_2.jpeg',
	    	"category_id" => 1,
	    	'created_at' => date("Y-m-d H:i:s")
	    ]);

	    DB::table('books')->insert([
	    	"name" =>  'Harry Potter and the Prisioner of Azkabam',
	    	"author" => 'J. K. Rowling',
	    	"user" =>  '',
	    	"publishedDate" =>  '2017-05-01',
	    	"image" =>  'HP_3.jpeg',
	    	"category_id" => 1,
	    	'created_at' => date("Y-m-d H:i:s")
	    ]);

	    DB::table('books')->insert([
	    	"name" =>  'Harry Potter and the Globet of Fire',
	    	"author" => 'J. K. Rowling',
	    	"user" =>  '',
	    	"publishedDate" =>  '2017-05-01',
	    	"image" =>  'HP_4.jpeg',
	    	"category_id" => 1,
	    	'created_at' => date("Y-m-d H:i:s")
	    ]);

	    DB::table('books')->insert([
	    	"name" =>  'Harry Potter and the Order of the Phoenix',
	    	"author" => 'J. K. Rowling',
	    	"user" =>  'Christian',
	    	"publishedDate" =>  '2017-05-01',
	    	"image" =>  'HP_5.jpeg',
	    	"category_id" => 1,
	    	'created_at' => date("Y-m-d H:i:s")
	    ]);

	    DB::table('books')->insert([
	    	"name" =>  'Harry Potter and the Half-Blood Prince',
	    	"author" => 'J. K. Rowling',
	    	"user" =>  'Ruben',
	    	"publishedDate" =>  '2017-05-01',
	    	"image" =>  'HP_6.jpeg',
	    	"category_id" => 1,
	    	'created_at' => date("Y-m-d H:i:s")
	    ]);

	    DB::table('books')->insert([
	    	"name" =>  'Harry Potter and the Deathly Hallows',
	    	"author" => 'J. K. Rowling',
	    	"user" =>  '',
	    	"publishedDate" =>  '2017-05-01',
	    	"image" =>  'HP_7.jpeg',
	    	"category_id" => 1,
	    	'created_at' => date("Y-m-d H:i:s")
	    ]);
    }

}
