'use strict';

var angular = require('angular');

angular.module('library')
.controller('homeController', require('./homeCtrl'))
.controller('addBookMdlController', require('./addBookMdlCtrl'))
.controller('udpBookMdlController', require('./udpBookMdlCtrl'));