angular.module('library')
.controller('udpBookMdlCtrl', [ '$scope','$uibModalInstance', 'homeService', '$rootScope', 'Book', '$filter',
function( $scope, $uibModalInstance, homeService, $rootScope, Book, $filter ){

	$scope.book = Book;

	$scope.publishedDate = new Date(  Book.publishedDate );
	
	// $scope.publishedDate = new Date(  $scope.publishedDate.getTime() - $scope.publishedDate.getTimezoneOffset() * -60000 );
	$scope.publishedDate = new Date(  $scope.publishedDate.getTime() - $scope.publishedDate.getTimezoneOffset() * -60000 );

    $scope.modal = {
        cerrar : function(){            
            $uibModalInstance.close('cancel');            
        }        
    }

    $scope.categories = [];

	$scope.category = [];

    $scope.loadCategories = function(){
		homeService.categories.get()
		.then(function( response ){
			if(response.data.code == 1){
				$scope.categories = response.data.categories;

			}else{
				errorMSG('Oops','Something goes Wrong.');
				
			}

		})
		.catch(function( response ){
			errorMSG('Oops','Something goes Wrong.');
			
		});
		
    }

    $scope.cleanUser = function(){
    	$scope.book.user = '';

    }

    // $scope.cleanUser();
    // alert('asdasd');

    $scope.updateBook = function(){

        $scope.book.category = $scope.category[0].id;
		$scope.book.publishedDate = $scope.publishedDate;

		homeService.books.update( $scope.book )
		.then(function( response ){
			if(response.data.code == 1){
				successMSG('Great!','Operation Successfully Completed');
				$rootScope.$broadcast('reloadBooks');
				$scope.modal.cerrar();

			}else{
				errorMSG('Oops','Something goes Wrong.');
				
			}

		})
		.catch(function( response ){
			errorMSG('Oops','Something goes Wrong.');
			
		});

	    
    }

}]);