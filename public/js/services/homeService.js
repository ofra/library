angular.module("library")
.factory('homeService', ['$rootScope','$http', function( $rootScope, $http ) {

    var urlBase = $rootScope.urlBase ;

    var dataFactory = {};

    var transform = function(data){
        return $.param(data);
    }

    dataFactory.books = {
        get : function(){
            return $http.get(urlBase + 'api/books');
        },
        delete : function(id){
            return $http.delete(urlBase + 'api/books/'+id);

        },
        update : function( data ){
            return $http.put(urlBase + 'api/books/update', data, {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                transformRequest: transform
            });
        }
    }

    dataFactory.categories = {
        get : function(){
            return $http.get(urlBase + 'api/categories');
        }
    }
 
    return dataFactory;

}]);
