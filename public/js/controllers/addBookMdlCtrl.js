angular.module('library')
.controller('addBookMdlCtrl', [ '$scope','$uibModalInstance', 'homeService', '$rootScope', 'fileUpload','$http',
function( $scope, $uibModalInstance, homeService, $rootScope, fileUpload, $http){

    $scope.categories = [];

	$scope.category = [];

	$scope.book = {
		name : '',
		author : '',
		publishedDate : '',
		source : '',
		category :  ''

	}

    $scope.modal = {
        cerrar : function(){            
            $uibModalInstance.close('cancel');            
        }        
    }

    $scope.loadCategories = function(){
		homeService.categories.get()
		.then(function( response ){
			if(response.data.code == 1){
				$scope.categories = response.data.categories;

			}else{
				errorMSG('Oops','Something goes Wrong.');
				
			}

		})
		.catch(function( response ){
			errorMSG('Oops','Something goes Wrong.');
			
		});

    }

    $scope.addBook = function(){

	    var file = $scope.myFile;
	       
       	var error = function(rs){
    		errorMSG('Oops',rs.msg);

       	}

        var uploadUrl = $rootScope.urlBase+"api/books";

        $scope.book.category = $scope.category[0].id;

        // console.log('books',$scope.book)
	    // fileUpload.uploadFileToUrl(file, $scope.book, uploadUrl, callback, error);
	    
	    var fd = new FormData();
	    fd.append('file', file);

	   	$http.post(uploadUrl, fd, {
	      transformRequest: angular.identity,
	      headers: {'Content-Type': undefined},
	      params: $scope.book
	   	})
	   	.then( function(rs){
    		if(rs.data.code == 1){
				successMSG('Great!','Operation Successfully Completed');
				$rootScope.$broadcast('reloadBooks');
				$scope.modal.cerrar();

			}else{
				errorMSG('Oops',rs.data.msg);
				
			}

	    } )
	    .catch(error);
    }	
    
}])
.directive('fileModel', ['$parse', function ($parse) {
    return {
       restrict: 'A',
       link: function(scope, element, attrs) {
          var model = $parse(attrs.fileModel);

          var modelSetter = model.assign;
          
          element.bind('change', function(){
             scope.$apply(function(){
                modelSetter(scope, element[0].files[0]);
             });
          });
       }
    };
 }])
.service('fileUpload', ['$http', function ( $http ) {
    this.uploadFileToUrl = function(file, params, uploadUrl, callback, error){
	    var fd = new FormData();
	    fd.append('file', file);

	   	$http.post(uploadUrl, fd, {
	      transformRequest: angular.identity,
	      headers: {'Content-Type': undefined},
	      params: params
	   	})
	   	.success( callback )
	    .error(error);
	}
}]);

