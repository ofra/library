require("normalize.css/normalize.css");
require("bootstrap/dist/css/bootstrap.css");
require("angular-loading-bar/src/loading-bar.css");
require("angular-multi-select/dist/prod/angular-multi-select.min.css");

var angular = require('angular');

require("angular-ui-bootstrap");
require('angular-route');	
require('angular-loading-bar');	
require("angular-ui-bootstrap");
require("angular-animate");
// require("lokijs");
require("angular-multi-select");

angular.module('library', ['ngRoute','angular-loading-bar','ngAnimate','ui.bootstrap','angular-multi-select'])
// angular.module('comprasApp',['ngRoute','angular-loading-bar','ngAnimate','datatables','datatables.bootstrap','ui.bootstrap','ngResource','datatables.columnfilter','ngDropzone','xeditable','isteven-multi-select','daterangepicker','datatables.scroller','datatables.buttons','angular-tour'])
.config(['$routeProvider', '$httpProvider','cfpLoadingBarProvider', '$locationProvider',
function( $routeProvider, $httpProvider, cfpLoadingBarProvider, $locationProvider ) {

	//-------------------------------------
	// $httpProvider.defaults.headers.post['Content-Type'] = 'application/json; charset=utf-8';
	//-------------------------------------
	cfpLoadingBarProvider.includeSpinner = true;
	cfpLoadingBarProvider.includeBar = 'true';
	//-------------------------------------
	$httpProvider.interceptors.push(function($rootScope) {
		return {
			request: function(config) {
				$rootScope.$broadcast('loading:show')
				return config
			},
			response: function(response) {
				$rootScope.$broadcast('loading:hide')
				return response
			}
		}
	});
	
	$locationProvider.html5Mode(false).hashPrefix('');

	//-------------------------------------
	$routeProvider
    .when('/', {
		templateUrl: 'templates/home.html',
		controller: 'homeCtrl',
		controllerAs : 'data',
	})
	.when('/melissa', {
		templateUrl: 'templates/oscar.html',
		// controller: 'homeCtrl',
		// controllerAs : 'data',
	})
	.otherwise({
		redirectTo: "/error/400"
	});
	//-------------------------------------

}])
.run(function($http, $location, $rootScope, cfpLoadingBar/*, DTDefaultOptions*/ ) {

	$rootScope.urlBase = 'http://localhost:8000/';
	
	/*	DTDefaultOptions.setLanguage({
			"sProcessing": "<i class='fa fa-refresh faa-spin animated' style='font-size:4em !important; color:#3a78b9;' ></i><br> <a style='color:#3a78b9;'>Procesando...</a>",
			"sLengthMenu": "Mostrar _MENU_ registros",
			"sZeroRecords": "No se encontraron resultados",
			"sEmptyTable": "NingÃºn dato disponible en esta tabla",
			"sInfo": "Registros del _START_ al _END_ de _TOTAL_ registros",
			"sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
			"sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
			"sInfoPostFix": "",
			"sSearch": "Buscar:",
			"sUrl": "",
			"sInfoThousands": ",",
			"sLoadingRecords": "Cargando...",
			"oPaginate": {
				"sFirst": "Primero",
				"sLast": "Ãšltimo",
				"sNext": "Siguiente",
				"sPrevious": "Anterior"
			},
			"oAria": {
				"sSortAscending": ": Activar para ordenar la columna de manera ascendente",
				"sSortDescending": ": Activar para ordenar la columna de manera descendente"
			}
		});*/

	$rootScope.start = function() {
		cfpLoadingBar.start();
	};

	$rootScope.complete = function () {
		cfpLoadingBar.complete();
		setTimeout(function(){
			// $rootScope.pageSetup();
		},1000);
	}

	$rootScope.$on('loading:show', function() {
		$rootScope.start();
	});

	$rootScope.$on('loading:hide', function() {
		$rootScope.complete();

	});
 	// $rootScope.pageSetup = pageSetup;

}).directive('validFile',function(){
  return {
    require:'ngModel',
    link:function(scope,el,attrs,ngModel){
      //change event is fired when file is selected
      el.bind('change',function(){
        scope.$apply(function(){
          ngModel.$setViewValue(el.val());
          ngModel.$render();
        })
      })
    }
  }
});



require('./controllers');
require('./services');
