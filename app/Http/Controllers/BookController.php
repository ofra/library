<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
Use App\Book;

use File;
use DateTime;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        try{
            // DB::beginTransaction();
            $rs = array();

            $books = DB::table( DB::raw('books as b') )
            ->select('b.id','b.name','b.author','b.user','b.publishedDate','b.image','b.category_id', 'c.name as category')
            ->join( DB::raw('categories as c'), 'b.category_id', '=', 'c.id' )
            ->get();


            foreach ($books as $book) {
                array_push($rs, array(
                    'id' => $book->id,
                    'name' => $book->name,
                    'author' => $book->author,
                    'user' => $book->user,
                    'publishedDate' => $book->publishedDate,
                    'source' => $book->image,
                    'borrowed' => trim($book->user) != '' ? 'borrowed' : 'available' , 
                    'category_id' => $book->category_id,
                    'category' => $book->category
                ));
            } 

            return Response()->json( array( 'code' => "1", 'books' => $rs ) );
           
            // DB::commit();
             
        }catch(\Illuminate\Database\QueryException  $e){
            // DB::rollback();
            return Response()->json( array('code' => '-1', 'msg' => 'Oops Something goes wrong.', 'error' => $e->getMessage() ) );
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){

        $data = $request->all();

        try{
            DB::beginTransaction();

            if( $request->hasFile('file') ) {
                $file = $request->file('file');

                $extension = trim( strtoupper($file->getClientOriginalExtension()) );

                if( $extension != 'PNG' && $extension != 'JPEG' && $extension != 'JPG' ){
                    return Response()->json(array('code'=> '-1', 'msg'=>'Just JPEG or PNG files are allowed.'));
                }

                $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                
                $charactersLength = strlen($characters);
                
                $randomString = '';
                for ($i = 0; $i < 10; $i++) {
                    $randomString .= $characters[rand(0, $charactersLength - 1)];
                }
                // Now you have your file in a variable that you can do things with
                $storeFolder =  realpath(public_path('storage/'));

                $imageName = $randomString.".".$file->getClientOriginalExtension();
                $rutaDeArchivo = $storeFolder."\\".$imageName;

                if( file_exists($rutaDeArchivo) ){
                    return Response()->json(array('code'=> '-1', 'msg'=>'File Already Exists.'));

                }
                
                $date = date('Ymd', strtotime($data['publishedDate']));

                $books = DB::table('books')
                ->insert([
                    "name" => $data['name'],
                    "author" => $data['author'],
                    "publishedDate" =>  $date ,
                    "image" => $imageName,
                    "category_id" => $data['category'],
                    'created_at' => date("Y-m-d H:i:s"),    
                    "user" => ''
                ]);

            }


            if( $books ){

                $file->move($storeFolder , $imageName);

                DB::commit();
                
                return Response()->json( array( 'code' => "1", 'msg' => 'Success' ) );

            }else{
                DB::rollback();
                return Response()->json( array('code' => '-1', 'msg' => 'Oops Something goes wrong when insert.' ) );
                
            }
            
        }catch(\Illuminate\Database\QueryException  $e){
            DB::rollback();
            return Response()->json( array('code' => '-1', 'msg' => 'Oops Something goes wrong.', 'error' => $e->getMessage() ) );
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    public function updateBook( Request $request ){
        $data = $request->all();

        try{
            DB::beginTransaction();

            $date = new DateTime(strtotime($data['publishedDate']));

            $book = DB::table('books')
            ->where('id','=', $data['id'] )
            ->update([
                "name" => $data['name'],
                "author" => $data['author'],
                "publishedDate" =>  $date ,
                "user" =>  isset( $data['user'] ) ? $data['user'] : '',
                "category_id" => $data['category'],
                'updated_at' => date("Y-m-d H:i:s"),    
            ]);

            if( $book ){

                DB::commit();
                
                return Response()->json( array( 'code' => "1", 'msg' => 'Success' ) );

            }else{
                DB::rollback();
                return Response()->json( array('code' => '-1', 'msg' => 'Oops Something goes wrong when update.' ) );
                
            }
            
        }catch(\Illuminate\Database\QueryException  $e){
            DB::rollback();
            return Response()->json( array('code' => '-1', 'msg' => 'Oops Something goes wrong.', 'error' => $e->getMessage() ) );
        }
    } 

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        DB::beginTransaction();

        try{

            // ******* Don't forget to delete the image ******

            $deleteBook = DB::table('books')->where('id','=',$id)->delete();

            if( $deleteBook ){

                DB::commit();
                
                return Response()->json( array( 'code' => "1", 'msg' => 'Success' ) );

            }else{
                DB::rollback();

                return Response()->json( array('code' => '-1', 'msg' => 'Oops Something goes wrong when delete.' ) );
                
            }

        }catch(\Illuminate\Database\QueryException  $e){
            DB::rollback();
            return Response()->json( array('code' => '-1', 'msg' => 'Oops Something goes wrong.', 'error' => $e->getMessage() ) );
        }

    }
}
