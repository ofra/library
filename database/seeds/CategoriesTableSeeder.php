<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('categories')->insert([
            'name' => 'Fantasy',
            'description' => 'The best one.',
            'created_at' => date("Y-m-d H:i:s")
        ]);

    	DB::table('categories')->insert([
            'name' => 'Drama',
            'description' => 'The saddest one.',
            'created_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('categories')->insert([
            'name' => 'Sci-fi',
            'description' => 'The most exciting.',
            'created_at' => date("Y-m-d H:i:s")
        ]);

    }
}
