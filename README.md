# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
	System to manage awesome Books
* Version 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

	System requirements
	*Http server(Wamp, xamp, nginx, etc..)
	* Composer
	* Webpack 
	* Stylus
	* Npm (Node.js).
	Important: Install in a global enviroment.
	
	To Set up please, following the next steps
	
	*	Clone the repository, in a folder from your http server.
	*	open a windows console o terminal and go to the project folder.
	*	Install laravel libraries, type: composer install
	*	Install Javascript libraries, type: npm install
	*	Create a bundle file, type: webpack 
	*	Create the main css file: type stylus public/css/master.styl
	
	*	In the root of the path, rename the file .env.example to .env 
		
	* 	Add a laravel key, type: php artisan key:generate
	
	*	Create a database named "library" encode type: "utf8_unicode_ci"
	* 	Fill the enviroment file renamed before(.env), with your database credentials. 
		DB_DATABASE=library
		DB_USERNAME=root
		DB_PASSWORD=''
		
	*	Create a cache file, type: php artisan config:cache
	
	*	Create your database tables, type: php artisan migrate
	*	Fill your database running your seeds: php artisan migrate:refresh --seed
	
	*	Open the system on the next url:
	
		http://localhost/{project_folder}/public
	
	

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact