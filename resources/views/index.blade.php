<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7;chrome=1" />
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
	<title>Document</title>
	<script src="https://use.fontawesome.com/343d7fd4be.js"></script>
	<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/lokijs/1.3.16/lokijs.min.js"></script>
	<link rel="stylesheet" href="./css/master.css">
	<link rel="stylesheet" href="./css/smart.css">

</head>
<body ng-app="library" >

	<div ng-view > </div>

	<footer>
		Powered by Maniak <i class="fa fa-hand-peace-o"></i>
	</footer>

	<script src="./js/includes/messages.js"></script>
	<script src="./js/built/app.bundle.js"></script>

</body>
</html>